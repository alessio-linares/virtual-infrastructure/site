# CLI

The main way a user interacts with VInfra is through the CLI application.

## Installation

You can install the VInfra CLI application with `pip`. Just run the following
command:

    pip install -U vinfra --extra-index-url https://gitlab.com/api/v4/projects/22750752/packages/pypi/simple

## Initial setup

If this is your first time using the vinfra CLI you might want to run `vinfra
config token` so that you don't have to pass the `--token` parameter every time.

This will store your raw token in a file in your computer. The application will
output the path of the file it is storing it in. In any case it should be in
`$HOME/.config/vinfra/config`.

#### Setting autocomplete

You may add the line bellow to your `.bashrc`:

    eval "$(register-python-argcomplete vinfra)"

For other shells check the [argcomplete
docs](https://kislyuk.github.io/argcomplete/) for instructions on how to set it
up.

## General usage

The `vinfra` CLI application is built to help you discover how to use it. At
any point you can add `-h` to your full or partial command and it will print a
comprehensive text of all available options and an explanation for what they
do.


Bellow you can find examples of common use cases:

#### Create a new resource

    vinfra resource create -f my-awesome-volume.yaml

#### Get resource definition schema for services

    vinfra resource schema service

#### Update existing resource with new definition

    vinfra resource update -f my-awesome-service.yaml

#### Remove existing resource

    vinfra resource rm 12-secret-my-db-password

#### Show the logs of a service

    vinfra service logs -t 100 53-service-my-super-api
