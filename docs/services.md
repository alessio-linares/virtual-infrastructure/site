# Services

Service resources represent a group of tasks (or replicas or instances, all
three terms will be used interchangeably) that run a docker image configured as
defined in the service resource definition file.

The replicas of a service are to be considered volatile and ephemeral. They can
be restarted at any point for any reason. A new replica will usually be created
before stopping an existing one but it may not always be the case and will for
sure not be the case with services that have mounts (as a [volume][volumes] can
only be mounted on one replica at a time). This is a constraint of the
environment. It is up to the user to overcome it. A common strategy is to have
more than one replica deployed.

Tasks of a service will always immediately restart in case of error.

## Updatability

Service resources can be updated.

The update will trigger a pull of the defined docker image and then will
replace all instances with the new definition.

The strategy for replacing instances works as follows:

- if service has mounts: stop the running instance and then start the new one
- otherwhise: start `max(1, int(num_replicas/3))` of the new version and then
  stop the same number of replicas of the old version. Repeat until no old
  replicas are left.

## Basic creation

Consider the following service resource definition file:

    # my-service.yaml
    account_id: 43
    type: service
    name: my-service
    image: jwilder/whoami
    cpu: 0.1
    memory: 200m

We can create the service by executing the command bellow:

    $ vinfra resource create -f my-service.yaml
    account_id: 43
    command: []
    cpu: 0.1
    environment: {}
    image: jwilder/whoami
    memory: 209715200b
    mounts: []
    name: my-service
    networks: []
    public: null
    replicas: 1
    secrets: []
    type: service

## Advanced usage

In addition to the required attributes defined in the example above, a service
can be configured even further by setting the non-required attributes.

#### command

`command` is a list of strings, by default empty. These are the arguments that
will be passed to the docker image in the same way they are passed when doing
`docker run <image> <command>`.

#### environment

`environment` is a mapping that defines the environment variables that will be
injected to the service tasks.

#### mounts

`mounts` is a list of mount definitions. A mount is a definition that defines
where in the file system of an instance is a [volume][volumes] to be mounted.

#### networks

`networks` is a list of [network][networks] resource ids. This defines the
[networks][networks] that the instances of the service will be connected to.

#### public

`public` sets the configuration of the service to be exposed through the
[service gateway][service-gateway].

If `public` is not present it defaults to `null`.

If `public` is `null`, the service will not be exposed through the gateway.

For more information on how to expose your HTTP service, check the [service
gateway documentation][service-gateway].

#### replicas

`replicas` defines the number of tasks that the service will be running.
Defaults to 1.

#### secrets

`secrets` is a list of [secrets][secrets] resource ids. This defines the
[secrets][secrets] that the instances of the service will have access to.

[volumes]: /docs/volumes "Volumes"
[networks]: /docs/networks "Networks"
[service-gateway]: /docs/service-gateway "Service Gateway"
[secrets]: /docs/secrets "Secrets"
