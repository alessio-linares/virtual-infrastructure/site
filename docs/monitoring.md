# Monitoring

VInfra exposes service metrics for resource usage through a Prometheus
API-compatible endpoint. This means you can connect your preffered monitoring
tools to it and check how your services are running.


# Grafana

![Grafana Metrics](/images/grafana-metrics.png)

You can connect VInfra metrics to your Grafana instance by adding it as a *Prometheus* Data Source.

First, go to the Data Sources settings page. You can get there by clicking on
the *Data Sources* option in the *Configuration* (gear) menu.
![Configuration -> Data Sources](/images/grafana-datasources.png)

Then, click on *Add data source*
![Add data source](/images/grafana-datasources-config-header.png)

and select *Prometheus* from the list.
![Add data source](/images/grafana-datasources-new-prometheus.png)

Now we are going to configure the data source.

First, give your data source a name (i.e. `vinfra account {account_id}`).

Then, set the URL of the data source to
`https://api.vinfra.alessio.cc/v1/metrics/{account_id}`, where `{account_id}`
is your VInfra account id number.
![Set URL](/images/grafana-datasources-new-prometheus-url.png)

Next, add a HTTP header to authorize the access to your metrics. For this
you'll need a [VInfra API token][api-token] for an [identity][vinfra-identity] with permission to access your
metrics. The header's name is `Authorization` and the value is `Bearer
{API_token}`, where `{API_token}` is your [VInfra API token][api-token].
![Auth header](/images/grafana-datasources-new-prometheus-auth.png)

Following that, set scrape interval to *1m* and the HTTP method to *GET*.
![HTTP GET method](/images/grafana-datasources-new-prometheus-GET.png)

Finally, click on *Save & Test*. You should get a message with a green
background similar to the one in the image bellow.
![Save & Test](/images/grafana-datasources-new-prometheus-save.png)

You can now start monitoring your [services][services] by importing the Grafana dashboard
found [here](/data/grafana-dashboard-services.json).


[api-token]: /docs/identities#api-token "VInfra API token"
[vinfra-identity]: /docs/identities "VInfra identity"
[services]: /docs/services "Services"
