# Service gateway

[Service][services] gateway is the name of the VInfra system that lets you
publish your public HTTP/HTTPS [services][services] to the Internet.

Using this [service][services] is as simple as adding a `public` definition to
your [service][services] resource definition file.

## Setting up your domain

Create a new A DNS record for your domain to point to your cluster's IP (this
is provided when the account is created).

## Basic usage

To expose an HTTP [service][services] to the public you can add the following
to your service definition:

    # my-service.yaml
    ...
    public:
      hostnames: [example.com]
      port: 80

That's it! Now update your [service][services] and you'll be able to access it
at the hostname defined there.

What this is doing is mapping the list of `hostnames` you are defining to your
[service][services] at port `80` (the port can be anything you want).

## TLS & HTTPS

`public` has an optional attribute named `tls`. If not present its value is
`null`, this means that the service will **not** be exposed using TLS/HTTPS.

If you want to expose your service through HTTPS, you must set a value for
`tls`. `tls` expects an object with an optional attribute `tls_only`.
`tls_only` defaults to `false`.

This means that if you want to expose your service through both, HTTP and HTTPS
you can set `public` to be like this:

    # my-service.yaml
    ...
    public:
      hostnames: [example.com]
      port: 80
      tls: {}

On the other hand, if you want to only expose your service through HTTPS:

    # my-service.yaml
    ...
    public:
      hostnames: [example.com]
      port: 80
      tls:
        tls_only: true

[services]: /docs/services "Services"
