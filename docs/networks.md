# Networks

Network resources represent virtual networks that you can create to privately
connect your [services][services]. Only [services][services] that you define that are on an existing
network will have access to other [services][services] in that network. Two [services][services] in
different networks won't be able to access each other.

[Services][services] in the same network can access each other by using [service][services] resource
id as hostname. For example, say we have a REST API `4-service-my-api` that
connects to a database `4-service-my-db`. The API can use `4-service-my-db` as
the database hostname to connect to it, no need for IPs or anything else. If
your [service][services] is replicated, the traffic will be loadbalanced automatically and
routed to the various instances of your [service][services].

## Updatability

Network resources can **not** be updated.

## Basic creation

Consider the following network resource definition file:

    # my-network.yaml
    type: network
    account_id: 13
    name: my-network

We can create the network by executing the command bellow:

    $ vinfra resource create -f my-network.yaml
    IPAM_pools:
    - gateway: 10.3.0.1/32
      ip_range: null
      subnet: 10.3.0.0/16
    account_id: 13
    internal: true
    name: my-network
    type: network

You can connect a [service][services] to the network by adding the network's resource id in
the `networks` list, in the [service][services]'s resource definition file:

    # my-service.yaml
    ...
    networks:
    - 13-network-my-network

## Advanced usage

You can set two other values in the network resource definition file:
`internal` and `IPAM_pools`.

#### internal

This setting controls whether [services][services] in this network will have access to the
public Internet.

By default, `internal` is set to `false`. This means that [services][services] in this
network will be able to access the public Internet.

If `internal` is set to `true`, [services][services] in this network will not be able to
access the Internet.

#### IPAM_pools

Do not set this value when creating a network unless you know what you are doing!

The value will be populated when creating the network and will inform you of
the subnet assigned to your newly created network.

[services]: /docs/services "Services"
