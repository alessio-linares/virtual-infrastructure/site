# Identities

An identity is contains a set of permission that lets (or prevents) users
access different to the resources and metrics of an account.

Typically a user will interact with VInfra by authenticating as an identity
using its [API token](#api-token).


<a name="api-token"></a>
## API token

An API token authorizes an interaction with VInfra as the identity the token
belongs to. An identity will have one token, that can be rotated if needed, and
a token will only authorize as one identity.
