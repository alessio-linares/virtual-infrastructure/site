# Secrets

Secret resources let you safely store delicate values, such as passwords or API
tokens, that will only be accessible to [services][services] you give access to.

## Updatability

Secret resources can **not** be updated.

## Basic creation

Consider the following secret resource definition file:

    # my-secret.yaml
    account_id: 57
    type: secret
    name: my-db-password
    value: my-super_s3cr3t p4sS

We can create the secret by executing the command bellow:

    $ vinfra resource create -f my-secret.yaml
    account_id: 57
    identifier: iyrhvxdibmn704x7gui150d63
    name: my-db-password
    type: secret
    value: my-super_s3cr3t p4sS

## Accessing secrets

You can give a [service][services] access to a secret by adding the secret's resource id in
the `secrets` list, in the [service][services]'s resource definition file:

    # my-service.yaml
    ...
    secrets:
    - 57-secret-my-db-password

The [service][services] can access the value by reading the contents of the file in
`/run/secrets/<secret_resource_id>`, in the above example it'd be
`/run/secrets/57-secret-my-db-password`.

[services]: /docs/services "Services"
