# Resources

A VInfra resource is anything you can create and manage through VInfra, such as
services.

You can create, delete, update, etc. your resources using the [VInfra CLI
application][vinfra_cli].

All resources are uniquely identified by its *Resource Id*, a string following
the form: `{account_id}-{resource_type}-{resource_name}`. Examples:

- A *network* in account *13* named *sports_database*:
  `13-network-sports_database`
- A *secret* in account *42* named *db_pass*: `42-secret-db_pass`
- A *service* in account *152* named *sports_api*: `152-service-sports_api`
- A *volume* in account *5* named *photos*: `5-volume-photos`

You can know more about the available resources by going to their specific
documentation page:

- [Cronjobs](/docs/cronjobs)
- [Networks](/docs/networks)
- [Secrets](/docs/secrets)
- [Services](/docs/services)
- [Volumes](/docs/volumes)


[vinfra_cli]: /docs/cli "VInfra CLI"
