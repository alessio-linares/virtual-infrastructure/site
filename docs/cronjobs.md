# Cronjobs

The main purpose of cronjobs is to enable running a certain task on a regular
schedule. This could be anywhere from a small script to, for example do an API
call, to complex batch jobs with multiple tasks running at the same time.

Cronjob resources represent a group of tasks (or replicas or instances, all
three terms will be used interchangeably) that run a docker image configured as
defined in the cronjob resource definition file.

These tasks will be started following the configured schedule and won't be
automatically restarted when they finish running. If a task is already running
when the next iteration is scheduled to run, the existing tasks will be left
running and no new tasks will be started.

## Updatability

Cronjob resources can be updated.

The update will trigger a pull of the defined docker image and then will
stop any tasks running, until the next schedule trigger.

## Basic creation

Consider the following cronjob resource definition file:

    # my-cronjob.yaml
    account_id: 43
    type: cronjob
    name: test
    image: backup-database:latest
    cpu: 1
    memory: 100m
    enabled: true
    schedule: '@weekly'


We can create the cronjob by executing the command bellow:

    $ vinfra resource create -f my-cronjob.yaml
    account_id: 43
    type: cronjob
    name: test
    image: backup-database:latest
    cpu: 1
    memory: 100m
    enabled: true
    schedule: '@weekly'

## Advanced usage

In addition to the required attributes defined in the example above, a cronjob
can be configured even further by setting the non-required attributes.

#### enabled

When `enabled` is set to `false` the cronjob won't be started. This allows to
have the cronjobs created but only enable them when needed.

#### schedule

A crontab expression. Must follow the [CRON expression
format](https://pkg.go.dev/github.com/robfig/cron?utm_source=godoc#hdr-CRON_Expression_Format)

#### command

`command` is a list of strings, by default empty. These are the arguments that
will be passed to the docker image in the same way they are passed when doing
`docker run <image> <command>`.

#### args

`args` is a list of strings, by default empty. These are the arguments that
will be passed to the docker image in the same way they are passed when doing
`docker run <image> <command> <args...>`.

#### environment

`environment` is a mapping that defines the environment variables that will be
injected to the cronjob tasks.

#### networks

`networks` is a list of [network][networks] resource ids. This defines the
[networks][networks] that the instances of the service will be connected to.

#### replicas

`replicas` defines the number of tasks that the service will be running.
Defaults to 1.

#### secrets

`secrets` is a list of [secrets][secrets] resource ids. This defines the
[secrets][secrets] that the instances of the service will have access to.

[networks]: /docs/networks "Networks"
[secrets]: /docs/secrets "Secrets"

