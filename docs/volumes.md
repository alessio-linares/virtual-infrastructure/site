# Volumes

Volume resources are storage units that you can attach to your [services][services] to
store persistent data. By default, anything you write to disk from your
[services][services] will be lost any time your [service][services] restart, for example but not
limited to when you update your [service][services]. Volumes let you overcome this by
persisting data accross [service][services] restarts, destruction, etc.

A volume can only be attached to one [service][services] task at a time. That means that
only one [service][services] can mount a volume at a time and said [service][services] can only have
one replica.

## Updatability

Volume resources can **not** be updated.


## Basic creation

Consider the following volume resource definition file:

    # my-volume.yaml
    account_id: 143
    type: volume
    name: my-volume
    size: 1024m

We can create the volume by executing the command bellow:

    $ vinfra resource create -f my-volume.yaml
    account_id: 143
    name: my-volume
    size: 1g
    type: volume

You can mount a volume to a [service][services] by adding the mount definition in the
`mounts` list, in the [service][services]'s resource definition file:

    # my-service.yaml
    ...
    mounts:
    - volume: 143-volume-my-volume
      path: /some/path

The volume will be mounted on the [service][services] at the *path* defined in the mount
definition. In the example above, the contents of the volume can be accessed by
the [service][services] by accessing the path `/some/path` in the filesystem.

[services]: /docs/services "Services"
