VInfra is a Platform as a Service (PaaS) that lets you create and manage
[resources](/docs/resources) such as services and secrets.

Check the [docs](/docs) to know more!
