<table style="width: 100%">
    <tr style="width: 100%">
        <td style="width: 50%">Find us in <a href="https://gitlab.com/alessio-linares/virtual-infrastructure/">Gitlab</a>!</td>
        <td style="width: 50%; text-align: right"></td>
    </tr>
    <tr style="width: 100%">
        <td style="width: 50%"><a href="https://gitlab.com/alessio-linares/virtual-infrastructure/site">Contribute to this site</a>!</td>
        <td style="width: 50%; text-align: right">&#169; Alessio Linares 2021</td>
    </tr>
</table>
